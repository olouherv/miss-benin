<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Votes;
use Response;

class VotesController extends Controller
{
    public function index()
    {
        //
        $votes = Votes::all();
        return response()->json($votes);
    }

    public function store(Request $request)
    {
        $vote = Votes::where('email',$request->email)->count();
        if($vote > 0){
            return Response::json(array('errors' => 'Ce compte a été déjà utilisé pour un vote.'));
        }else{
            $vote = new Votes();
            $vote->nom_complet = $request->nom_complet;
            $vote->choix = $request->choix;
            $vote->email = $request->email;
            $vote->date = today();
            $vote->save();
            return Response::json($vote);
        }
    }
}
