<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votes extends Model
{
    //
    
    public $timestamps = false;

    protected $fillable = [
    	'id',
    	'nom_complet',
    	'choix',
    	'email',
    	'date'
    ];
}
