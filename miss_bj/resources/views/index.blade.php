<!DOCTYPE html>
<html>
<head>
	<title>Miss Bénin 2019</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>
	<meta name="google-signin-client_id" content="579905625105-jcll0sbvgkhc0bdk7efdf8k25tfr3nio.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick-theme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick.css')}}">
	<!--script src="{{asset('assets/js/jquery-3.3.1.slim.min.js')}}"></script-->
	<script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color: rgba(0,0,0,0.4);">
		<a class="navbar-brand" href="#">
			<img src="assets/img/miss_benin_logo.png" width="100" height="60" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto text-center">
				<li class="nav-item active">
					<a class="nav-link" href="#">ACCUEIL</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#candidates_section">CANDIDATES</a>
				</li>
				<li class="nav-item">
					<a class="nav-link btn_liste_votes" href="#">VOTES</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#commite_section">COMITE</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contact_section">CONTACT</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#partenaires_section">PARTENAIRES</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="img-home">
		<div class="content text-center">
			<h4 class="title">Célébrons ensemble la beauté !</h4>
			<a href="#candidates_section" class="btn btn-lg btn-primary">
				Voir les candidates <i class="fa fa-arrow-down"></i>
			</a>
		</div>
	</div>
	<section class="container-fluid" id="candidates_section">
		<h1 class="text-center" style="color: white; margin-bottom: 2em">
			CANDIDATES
		</h1>
		<div class="container mt-40">
            <div class="row mt-30 candidates">
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img id= "img1" src="{{asset('assets/img/candidate1.jpg')}}" class="card-img-top" alt="candidate 1">
                        <ul class="icon">
                            <li><a class="text-center btn_info" data-candidate="1" data-nom="BONOU" data-prenom="JOANA" data-dep="ATLANTIQUE" data-age="21" data-ecole="ENEAM" data-filiere="STATISTIQUE" data-taille="1m85"><i class="fa fa-info"></i></a></li>
                            <li><a class="text-center btn_vote" data-candidate="1"><i class="fa fa-heart"></i></a></li>
                        </ul>
                        <div class="box-content text-center">
                            <h3 class="title">JOANA BONOU</h3>
                            <span class="post">Candidate 1</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img id= "img2" src="{{asset('assets/img/candidate2.jpg')}}" class="card-img-top" alt="candidate 2">
                        <ul class="icon">
                            <li><a class="text-center btn_info" data-candidate="2" data-nom="VIGNON" data-prenom="RACHELLE" data-dep="ZOU" data-age="20" data-ecole="IFRI" data-filiere="SIRI" data-taille="1m75"><i class="fa fa-info"></i></a></li>
                            <li><a class="text-center btn_vote" data-candidate="2"><i class="fa fa-heart"></i></a></li>
                        </ul>
                        <div class="box-content text-center">
                            <h3 class="title">RACHELLE VIGNON</h3>
                            <span class="post">Candidate 2</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img id= "img3" src="{{asset('assets/img/candidate3.jpg')}}" class="card-img-top" alt="candidate 3">
                        <ul class="icon">
                            <li><a class="text-center btn_info" data-candidate="3" data-nom="METONOU" data-prenom="ARMELLE" data-dep="OUEME" data-age="25" data-ecole="ENAM" data-filiere="ADMINISTRATION DES FINANCES" data-taille="1m73"><i class="fa fa-info"></i></a></li>
                            <li><a class="text-center btn_vote" data-candidate="3"><i class="fa fa-heart"></i></a></li>
                        </ul>
                        <div class="box-content text-center">
                            <h3 class="title">ARMELLE METONOU</h3>
                            <span class="post">Candidate 3</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img id= "img4" src="{{asset('assets/img/candidate4.jpg')}}" class="card-img-top" alt="candidate 4">
                        <ul class="icon">
                            <li><a class="text-center btn_info" data-candidate="4" data-nom="SOGNON" data-prenom="NATHALIE" data-dep="ATLANTIQUE" data-age="25" data-ecole="FSS" data-filiere="PHARMACIE" data-taille="1m81"><i class="fa fa-info"></i></a></li>
                            <li><a class="text-center btn_vote" data-candidate="4"><i class="fa fa-heart"></i></a></li>
                        </ul>
                        <div class="box-content text-center">
                            <h3 class="title">NATHALIE SOGNON</h3>
                            <span class="post">Candidate 4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class>
			<!-- Button trigger modal -->
			<div class="text-center">
				<button type="button" class="btn btn-primary btn-lg btn_liste_votes">
				  Voir les votes
				</button>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="liste_votes">
			    <div class="modal-dialog modal-dialog-centered modal-lg">
			  		<div class="modal-content" style="background-color: rgba(255,255,255,0.9);">
			      		<div class="modal-header">
			        		<h5 class="modal-title" id="exampleModalCenterTitle">Liste des votes</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body center" id="table_space">
							<table class='table table-hover' id='votes' style='width: 100%'>
								<thead>
									<tr><th class='th-sm'>#</th><th class='th-sm'>NOM</th><th class='th-sm'>EMAIL</th><th class='th-sm'>CHOIX</th><th class='th-sm'>DATE</th><th class='th-sm'></th></tr>
								</thead>
								<tbody id='table_content'>
									
								</tbody>
							</table>
							<div id="table_script"></div>
			      		</div>
			    	</div>
			  	</div>
			</div>
		</div>
	</section>

	<section class="container-fluid" id="commite_section">
		<h1 class="text-center" style="color: white; margin-bottom: 2em">
			COMITE
		</h1>
		<div class="container mt-40">
            <div class="row mt-30 candidates">
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img src="{{asset('assets/img/kidjo.jpg')}}" class="card-img-top" alt="...">
                        <div class="box-content text-center">
                            <h3 class="title">MME KIDJO</h3>
                            <span class="post">PRESIDENTE</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img src="{{asset('assets/img/hounsou (2).png')}}" class="card-img-top" alt="...">
                        <div class="box-content text-center">
                            <h3 class="title">MR HOUNSOU</h3>
                            <span class="post">VICE PRESIDENT</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img src="{{asset('assets/img/usher.png')}}" class="card-img-top" alt="...">
                        <div class="box-content text-center">
                            <h3 class="title">MR USHER</h3>
                            <span class="post">TRESORIER</span>
                        </div>
                    </div>
                </div>                
                <div class="col-md-3 col-sm-6">
                    <div class="box17">
                        <img src="{{asset('assets/img/lupita.jpg')}}" class="card-img-top" alt="...">
                        <div class="box-content text-center">
                            <h3 class="title">MME NYONG'O</h3>
                            <span class="post">SECRETAIRE</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>

	<section class="container-fluid" id="contact_section">
		<h1 class="text-center" style="color: white; margin-bottom: 2em">
			CONTACT
		</h1>
		<div class="container">
			<div class="row" style="background-color: rgba(0,0,0,0.2);">
				<div class="col-lg-6 col-md-12">
					<form style="margin: 20px;">
						<div class="form-row" style="margin:50px 0px 50px 30px;">
							<div class="col">
						      	<input type="text" class="form-control" placeholder="Nom et prénom" required>
						    </div>
						</div>
						<div class="form-row" style="margin:50px 0px 50px 30px;">
							<div class="col">
						      	<input type="email" class="form-control" placeholder="Email" required>
						    </div>
						</div>
						<div class="form-row" style="margin:50px 0px 50px 30px;">
							<div class="col">
								<textarea class="form-control" placeholder="Votre message" required></textarea>
							</div>
						</div>
						<div class="form-row pull-right" style="margin:0px 0px 50px 30px;">
							<div>
								<button type="submit" class="btn btn-primary btn-lg">Envoyer</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="google-map text-center">
						<iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJn6wOs6lZwokRLKy1iqRcoKw" allowfullscreen="">
						</iframe>
						<h5 style="color: white;"> Suivez - Nous </h5>
						<div style="padding: 10px">
							<a class="btn btn-default social"><span class="fa fa-facebook"></span></a>
							<a class="btn btn-default social"><span class="fa fa-instagram"></span></a>
							<a class="btn btn-default social"><span class="fa fa-google"></span></a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>

	<section class="container-fluid text-center" id="partenaires_section">
		<h1 style="color: white; margin-bottom: 2em">
			PARTENAIRES
		</h1>
		<div class="container">
			<div class="row partenaires">
				<div class="col">
					<img src="{{asset('assets/img/presidence.png')}}" >
				</div>
				<div class="col">
					<img src="{{asset('assets/img/dayang.png')}}" >
				</div>
				<div class="col">
					<img src="{{asset('assets/img/moov.png')}}" >
				</div>
				<div class="col">
					<img src="{{asset('assets/img/ecobank.svg.png')}}" >
				</div>
				
			</div>			
		</div>
	</section>
	
	<footer style="background-color: black; padding: 10px;">
		<div class="container text-center">
			<span>Miss Bénin © 2019. Tous droits réservés.</span>
		</div>
	</footer>

	<!-- Modal -->
	<div class="modal fade candidate-informations">
  		<div class="modal-dialog modal-dialog-centered modal-lg">
    		<div class="modal-content">
      			<div class="modal-body">
      				<div class="container">
      					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float: right; color: white">
		          			<span aria-hidden="true">&times;</span>
		        		</button>
      					<div class="row" style="padding: 10px;">
      						<div class="col-md-4 col-sm-12">
      							<img id="img_candidate" class="card-img-top">
      							<h5 id="candidate"class="text-center" style="background: #71670E; color: white; padding: 10px;"></h5>
      						</div>
      						<div class="col-md-8 col-sm-12">
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Nom :</h5></div>
      								<div class="col candidate-info"><p id="nom"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Prénom :</h5></div>
      								<div class="col candidate-info"><p id="prenom"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Département :</h5></div>
      								<div class="col candidate-info"><p id="dep"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Age :</h5></div>
      								<div class="col candidate-info"><p id="age"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Ecole :</h5></div>
      								<div class="col candidate-info"><p id="ecole"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Filière :</h5></div>
      								<div class="col candidate-info"><p id="fil"></p></div>
      							</div>
      							<div class="form-row">
      								<div class="col candidate-info"><h5>Taille :</h5></div>
      								<div class="col candidate-info"><p id="taille"></p></div>
      							</div>
      						</div>
      					</div>
      					<button class="btn btn-info btn_vote" type="button" style="float: right; margin-bottom: 5px">
			        		<a><span class="fa fa-heart"></span>  Voter</a>
			        	</button>
      				</div>
      			</div>
    		</div>
  		</div>
	</div>

	<div class="modal fade candidate-vote">
  		<div class="modal-dialog modal-dialog-centered">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title">Option de vote</h5>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          		<span aria-hidden="true">&times;</span>
		        	</button>
    			</div>
      			<div class="modal-body text-center">
      				<input type="input" id="candidate_choisie" hidden="">
      				<div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style="margin-left: 35%"></div>
      			</div>
    		</div>
  		</div>
	</div>


	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/js/popper.min.js')}}"></script>
	<script src="{{asset('assets/js/slick.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('assets/js/vote.js')}}"></script>
	<script type="text/javascript">
		function signOut() {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function () {

			});
		}

		function onSignIn(googleUser){
			var profile = googleUser.getBasicProfile();
			var choix = $('#candidate_choisie').val();
			var CSRF_TOKEN = $('meta[name="_token"]').attr('content');


			$.ajax({
				type: 'POST',
				url: 'votes',
				data: {
					_token: CSRF_TOKEN,
					nom_complet: profile.getName(), 
					email: profile.getEmail(), 
					choix: choix
				},
				dataType: 'json',
				success: function(data){

					if(data.choix){
						alert("Succès : Vote enregistré pour la candidate " + data.choix);
					}else{
						alert("Erreur : Ce compte a été déjà utilisé pour un vote.");
					}

				}

			});

			signOut();
			sessionStorage.clear();
			$('.candidate-vote').modal("hide");
		}

		$('.btn_vote').click(function(){
			var candidate = $(this).data('candidate');
			$('.candidate-vote').modal("show");

			$('#candidate_choisie').val(candidate);

		});

		$('.candidates').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		    ]
		});

		$('.partenaires').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
		});

		$('.btn_info').click(function(){
			var candidate = $(this).data('candidate');
			$('.candidate-informations').modal("show");
			if(candidate == "1"){
				$("#img_candidate").attr("src",$('#img1').attr("src"));
			}else if(candidate == "2"){
				$("#img_candidate").attr("src",$('#img2').attr("src"));
			}else if(candidate == "3"){
				$("#img_candidate").attr("src",$('#img3').attr("src"));
			}else if(candidate == "4"){
				$("#img_candidate").attr("src",$('#img4').attr("src"));
			}

			$("#candidate").text("CANDIDATE " + candidate);
			$('#nom').text($(this).data('nom'));
			$('#prenom').text($(this).data('prenom'));
			$('#age').text($(this).data('age'));
			$('#fil').text($(this).data('filiere'));
			$('#taille').text($(this).data('taille'));
			$('#dep').text($(this).data('dep'));
			$('#ecole').text($(this).data('ecole'));

			$('button.btn_vote').attr('data-candidate', candidate);

		});
	</script>
</body>
</html>