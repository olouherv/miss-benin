$(document).ready(function(){
	$('.btn_vote').click(function(){
			var candidate = $(this).data('candidate');
			$('.candidate-vote').modal("show");
			$('#candidate_choisie').val(candidate);
		});

		$('.btn_info').click(function(){
			$('.candidate-informations').modal("show");
		});

		$('#btn_liste_votes').click(function(){
			$('#liste_votes').modal("show");
			$.ajax({
				type: 'GET',
				url: 'votes',
				dataType: 'json',
				success: function(data){
					$.each(data, function(i,item){
						$('#table_content').append("<tr><td>"+item.id+"</td><td>"+item.nom_complet+"</td><td>"+item.email+"</td><td>"+item.choix+"</td><td>"+item.date+"</td><td><button class='btn btn-info btn-sm'><span class='fa fa-edit'></span></button><button class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></button></td></tr>");
					});
				}
			});
			$("#table_space").append('<script type="text/javascript"> $("#votes").dataTable({ scrollY": "50vh","scrollX": true, "language": {"sProcessing": "Traitement en cours ...","sLengthMenu": "Afficher _MENU_ lignes","sZeroRecords": "Aucun résultat trouvé","sEmptyTable": "Aucune donnée disponible","sInfo": "Lignes _START_ à _END_ sur _TOTAL_","sInfoEmpty": "Aucune ligne affichée","sInfoFiltered": "(Filtrer un maximum de_MAX_)","sInfoPostFix": "",	"sSearch": "Rechercher :","sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Chargement...", "oPaginate": {"sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"},"oAria": {"sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"}}});</script>'
			);
		});


	function onSignIn(googleUser){
			var profile = googleUser.getBasicProfile();
			var choix = $('#candidate_choisie').val();
			var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
            

			$.ajax({
				type: 'POST',
				url: 'votes',
				data: {
					_token: CSRF_TOKEN,
					nom_complet: profile.getName(), 
					email: profile.getEmail(), 
					choix: choix
				},
				dataType: 'json',
				success: function(data){

					if(data.errors){
						//toastr.error('Erreur', 'Error Alert', data.errors);
						alert("Erreur : " + data.errors)
					}else{
						alert("Succès : Vote enregistré pour la candidate " + data.choix);
					}

				}

			});

			signOut();
			$('.candidate-vote').modal("hide");
		}

		function signOut() {
		    var auth2 = gapi.auth2.getAuthInstance();
		    auth2.signOut().then(function () {
		    	
		    });
		}
});