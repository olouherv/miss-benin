$(document).ready(function(){

	$('.btn_liste_votes').click(function(){
		$('#liste_votes').modal("show");
		$.ajax({
			type: 'GET',
			url: 'votes',
			dataType: 'json',
			success: function(data){
				$('#table_content').html("");
				$.each(data, function(i,item){
					$('#table_content').append("<tr><td>"+item.id+"</td><td>"+item.nom_complet+"</td><td>"+item.email+"</td><td>"+item.choix+"</td><td>"+item.date+"</td><td><button class='btn btn-info btn-sm'><span class='fa fa-edit'></span></button><button class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></button></td></tr>");
				});

				$("#table_script").html('<script type="text/javascript"> $("#votes").dataTable({ "scrollY": "50vh","scrollX": true, "language": {"sProcessing": "Traitement en cours ...","sLengthMenu": "Afficher _MENU_ lignes","sZeroRecords": "Aucun résultat trouvé","sEmptyTable": "Aucune donnée disponible","sInfo": "Lignes _START_ à _END_ sur _TOTAL_","sInfoEmpty": "Aucune ligne affichée","sInfoFiltered": "(Filtrer un maximum de_MAX_)","sInfoPostFix": "",	"sSearch": "Rechercher :","sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Chargement...", "oPaginate": {"sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"},"oAria": {"sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"}}});</script>'
					);
			}
		});
	});

	
});